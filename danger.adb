-- Filename: danger.adb
-- Description: A scenario with different ending depending on user input

		-- **** Preprocesor **** 


with Ada.Text_IO; -- Include I/O standard Library for strings
use Ada.Text_IO; -- Automaticaly detect Ada.Text_IO for string
with Ada.Integer_Text_IO; -- Include I/O Standard Library for integer
use Ada.Integer_Text_IO; -- Automaticaly detect Ada.Integer_Text_IO;



		-- **** danger procedure ****


procedure danger is -- Define procedure called danger
	choice : Integer; -- Declare an integer called choice
begin -- Begin of the danger procedure



		-- **** Introduction ****


		Put_Line ("You are on a skateboard. You are going really fast.");
		Put_Line ("Then suddenly, you see a baby crossing the road recklessly. You can only:");
		Put_Line ("1 Go Left");
		Put_Line ("2 Go right");
		Put_Line ("3 Go straight");
		Put ("Choose an option: ");
		Get (choice); 
		Put_Line ("");

		-- **** Options ****

		if choice = 1 then
			Put_Line ("You decide to go left. You quickly cross the road avoiding the baby, fall down and brake your tooth. At least, the baby is safe");
		elsif choice = 2 then
			Put_Line ("You decide to go right. You avoid the baby but get crashed by a car. You are actually at the hospital hungry, but the baby is safe.");
		elsif choice = 3 then
			Put_Line ("You decide to go straight. The baby look at you run away to where he came from. You reduced your actual velocity and decide to continue by walking");
		else
			Put_line ("You don't do anything. You came really close to the baby and do a kick flip and jumped over him. Then you realise you were actually lucid dreamming");
		end if;
		
		Put_Line("");
		Put_Line("Congratulation, you didn't killed anyone XD");
end danger;
-- Include I/O standard Library

